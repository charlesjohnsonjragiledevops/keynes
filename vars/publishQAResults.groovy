#!groovy

import jenkins.model.Jenkins

def call(Map qa) {
  qa.publishers.each { name, config ->
    if (config.enabled && Jenkins.instance.getPluginManager().getPlugin(config.plugin)) {
      method = config.method ? config.method : name
      def result = "${method}" config.params
      if (config.plugin == 'warnings-ng') {
        recordIssues(
          enabledForFailure: true,
          tool: result,
        )
      }
    }
  }
}
