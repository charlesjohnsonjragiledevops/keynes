#!groovy

@Grab(group='org.yaml', module='snakeyaml', version='1.18')

import org.yaml.snakeyaml.Yaml

/**
 * Returns a ConfigObject populated with the contents of a yaml file, if it does exist.
 */
@NonCPS
ConfigObject call(String path = null, String text = null) {
  ConfigObject config

  if (path) {
    def file = new File(path)
    if (file.exists()) {
      text = file.text
    }
  }

  if (text) {
    config = new Yaml().loadAs(text, ConfigObject)
  }
  else {
    config = new ConfigObject()
  }

  return config
}

