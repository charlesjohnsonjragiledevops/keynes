#!groovy

import jenkins.model.Jenkins

void declareConfigFiles(def files, String prefix = '') {
  files.each { k, item ->
    configFiles {
      customConfig {
        id item.id
        name item.name
        comment item.comment
        content item.content
      }
    }
  }
}

void declareFolders(def folders) {
  folders.each { k, item ->
    folder(item.name) {
      displayName item.displayName
      description item.description
    }
  }
}

void declareJobs(def jobs) {
  jobs.each { k, item ->
    // If the job already exists, respect it's disabled state.
    isDisabled = !item.config.enabled
    job = Jenkins.instance.getItemByFullName(item.name)
    if (job) {
      isDisabled = job.isDisabled()
    }
    pipelineJob(item.name) {
      displayName item.label
      description item.description
      disabled isDisabled
      logRotator {
        daysToKeep         item.config.daysToKeep.toInteger()
        numToKeep          item.config.numToKeep.toInteger()
        artifactDaysToKeep item.config.artifactDaysToKeep.toInteger()
        artifactNumToKeep  item.config.artifactNumToKeep.toInteger()
      }
      parameters {
        booleanParam('RECONFIGURE_PIPELINE', true)
      }
      definition {
        cps {
          script item.script
          sandbox()
        }
      }
    }

    queue(item.name)
  }
}

declareConfigFiles(seedConfigFiles)
declareFolders(seedFolders)
declareJobs(seedJobs)

