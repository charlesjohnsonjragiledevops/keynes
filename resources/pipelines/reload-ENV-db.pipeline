#!groovy
@Library('keynes') _

/* Job vars */
def LOCK = env.JOB_NAME
Integer TIMEOUT = CONFIG.timeout
String TIMER = CONFIG.timer
def UPSTREAM = CONFIG.upstream
if (UPSTREAM instanceof String) {
  UPSTREAM = [
    upstreamProjects: CONFIG.upstream,
    threshold: "SUCCESS",
  ]
}
def ANSIBLE_EXTRA_VARS_LOCAL
String ENVIRONMENT
String TARGET
def ENVIRONMENTS = VARS.environments.findAll{it.value}
def DATABASES = [:]
Boolean ARTIFACT

if ( !params.RECONFIGURE_PIPELINE ) {
  /* Runtime vars */
  ENVIRONMENT = params.environment
  TARGET = ENVIRONMENTS[ENVIRONMENT]['target']
  Boolean SANITIZE = getBoolean(params.sanitize)
  ARTIFACT = getBoolean(params.artifact)
  TARGET = CONFIG.inventory_maps[TARGET] ?: TARGET

  /* Populate job vars */
  LOCK = "${CONFIG.name}-${ENVIRONMENT}"
  VARS.databases.findAll{it.value}.each { dbname, dbconfig ->
    defaults = [
      key: dbname,
      db_host: VARS.db_host,
      db_file_import: "sqldumps/${dbname}.sql",
      db_file_dump: "sqldumps/${dbname}.sql",
      sanitize_tasks: VARS.sanitize_tasks,
      duplicity_source_url: VARS.duplicity_source_url,
      duplicity_file_to_restore: "${VARS.duplicity_sqldump_dir}/${dbname}.sql",
    ]
    DATABASES << ["${dbname}": defaults + dbconfig]
  }
  ANSIBLE_EXTRA_VARS_LOCAL=[
    appenv: ENVIRONMENT,
    target: TARGET,
    dump: ARTIFACT,
    sanitize: SANITIZE,
    concurrency: VARS.concurrency,
    databases: DATABASES,
  ]
}

pipeline {
  agent any
  options {
    skipDefaultCheckout()
    ansiColor('xterm')
    lock(LOCK)
    timeout(time: TIMEOUT, unit: 'MINUTES')
  }
  triggers {
    cron(TIMER)
    upstream(
      upstreamProjects: UPSTREAM.upstreamProjects,
      threshold: UPSTREAM.threshold,
    )
  }
  stages {
    stage('init') {
      steps {
        script {
          currentBuild.description = "Reload db on ${ENVIRONMENT} (${TARGET}): ${currentBuild.description ?: '-'}"
        }
        script {
          // Create sqldumps/ directory.
          sh 'mkdir -p sqldumps'
          sh 'chmod 777 sqldumps'
        }
      }
    }
    stage('validate') {
      steps {
        script {
          if (params.environment == params.environment_src) {
            error "Can't reload an environment over itself."
          }
        }
      }
      when {
        expression {
          return params.source == 'From another environment'
        }
      }
    }
    stage('extract-backup') {
      steps {
        script {
          DATABASES.each { dbname, dbconfig ->
            lock(resource: dbconfig.duplicity_source_url) {
              sshagent(credentials: [VARS.duplicity_credentials_id], ignoreMissing: true) {
                withEnv(["PASSPHRASE=${VARS.duplicity_pass}"]) {
                  sh "duplicity restore --file-to-restore ${dbconfig.duplicity_file_to_restore} ${dbconfig.duplicity_source_url} ${dbconfig.db_file_import}"
                  echo "NOTE: It's fine to get «Error '[Errno 1] Operation not permitted: '(...)'' processing .». It is due it can't restore file ownership"
                  // Extracted file is owned by jenkins with 0400 permissions.
                  // We need to give read access to all, because ansible playbook is run as deploy user.
                  // We also need to give write access, since the after-import dump overwrites this file.
                  // @TODO@ perform duplicity operation via ssh, or integrate duplicity support in the db-reload playbook
                  sh "chmod ugo+rw ${dbconfig.db_file_import}"
                  // Tail extracted file to get Dump info
                  sh "tail -n1 ${dbconfig.db_file_import}"
                }
              }
            }
          }
        }
      }
      when {
        expression {
          return params.source == 'From backup'
        }
      }
    }
    stage('dump-source') {
      steps {
        script {
          ENVIRONMENT = params.environment_src
          TARGET = ENVIRONMENTS[ENVIRONMENT]['target']
          TARGET = CONFIG.inventory_maps[TARGET] ?: TARGET
          def overrides = [
            appenv: ENVIRONMENT,
            target: TARGET,
            dump: true,
            concurrency: VARS.concurrency,
          ]
          lock("${CONFIG.name}-${ENVIRONMENT}") {
            runAnsiblePlaybook(CONFIG.ansible, VARS.playbook, ANSIBLE_EXTRA_VARS_LOCAL + overrides, VARS.credentials_id, '--tags dump')
          }
        }
      }
      when {
        expression {
          return params.source == 'From another environment'
        }
      }
    }
    stage('reload') {
      steps {
        script {
          CONFIG.ansible.extra_files = (CONFIG.ansible.extra_files?:[]) + VARS.tasks
          runAnsiblePlaybook(CONFIG.ansible, VARS.playbook, ANSIBLE_EXTRA_VARS_LOCAL, VARS.credentials_id)
          DATABASES.each { dbname, dbconfig ->
            if (ARTIFACT) {
              sh "tar -czf ${dbconfig.db_file_dump}.tar.gz ${dbconfig.db_file_dump}"
              archiveArtifacts artifacts: "${dbconfig.db_file_dump}.tar.gz", fingerprint: true
            }
          }
        }
      }
    }
  }
  post {
    cleanup {
      script {
        if (CONFIG.cleanWS) {
          cleanWs()
        }
      }
    }
  }
}

// vi:syntax=groovy
